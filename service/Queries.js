const Pool = require('pg').Pool

const pool = new Pool ({
    user:'postgres',
    host:'192.168.40.150',
    database:'testDB',
    password:'postgres',
    port: 5432
})
pool.connect()
const getdata=(req,res)=>
{
    pool.query('select * from People',(err,result)=>
    {
        if(err){
            throw err
        }
        res.status(200).json(result.rows)
    })
}
const insertdata =(req,res)=> {
  
    const {Firstname,lastname}=req.body
console.log("inserting");
    pool.query('insert into People(Firstname,lastname) values($1,$2)',[Firstname,lastname],(err,result)=>{
        if(err){
            console.log(err)
        }
        res.status(200).send('value added successfully')
    })
}
 
module.exports = {getdata,insertdata};
